# -*- coding: utf-8 -*-
#importation des différents modules
from tkinter import *
from PIL import Image
from screen import *

from observer import Observer
from generator import *

#classe Controller_General
class Controller_General(Observer):
    def __init__(self,parent,subject, subject_2, screen, lissajou_color):
        self.parent = parent
        self.screen = screen
        self.frame = Frame(parent)
        self.lissajou_color = lissajou_color
        self.lis_frame = Frame(self.frame)
        self.lissajou_visibility = IntVar()
        self.toggleLissajou = Checkbutton(self.lis_frame, text="Lissajou curve", fg = lissajou_color, command=self.update_lissajou_visibility, variable=self.lissajou_visibility)
        self.toggleLissajou.select()
        self.subject=subject
        self.subject_2 = subject_2

        self.lissajou_on_secondary = IntVar()
        self.place_lissajou_on_secondary = Checkbutton(self.lis_frame, text="Ouvrir un second Fenêtre", command=self.update_lissajou_on_secondary, variable=self.lissajou_on_secondary)

        self.spot_index=IntVar()
        self.scale_spot_index=Scale(self.frame,variable=self.spot_index,
                          label="Spot index",
                          orient="horizontal",length=250,
                          showvalue=0,from_=1,to=len(self.subject.get_signal())-1,
                          sliderlength=20,
                          command=self.update_spots)

        self.secondary_window = Toplevel(self.parent)
        self.secondary_window.protocol("WM_DELETE_WINDOW", self.hide_secondary)
        # Cacher la fenêtre secondaire
        self.secondary_window.withdraw()

        self.secondary_screen = Screen(self.secondary_window,
                self.subject,self.subject_2,
                False, False, True, fg_lissajou=self.lissajou_color)
        self.subject.attach(self.secondary_screen)
        self.subject_2.attach(self.secondary_screen)

    def update(self,subject):
        pass

    #fonction permettant de gérer l'affichage du signal lissajou dans la fenêtre principale
    def update_lissajou_visibility(self):
        self.screen.set_show_lissajou(self.lissajou_visibility.get() == 1)

    #fonction permettant la mise à jour du signal lissajou sur la fenêtre secondaire    
    def update_lissajou_on_secondary(self):
        if (self.lissajou_on_secondary.get() == 1):
            # Cacher la fenêtre secondaire
            self.secondary_window.deiconify()
        else:
            self.hide_secondary()

    #fonction permettant de gérer l'affichage de la fenêtre secondaire
    def hide_secondary(self):
        self.secondary_window.withdraw()
        if (self.lissajou_on_secondary.get() == 1):
            self.place_lissajou_on_secondary.deselect()

    #fonction permettant la mise à jour des spots
    def update_spots(self,event):
        self.screen.update_spots(self.spot_index.get())
        self.secondary_screen.update_spots(self.spot_index.get())

    def packing(self, side) :
        self.frame.pack(side=side, expand=1, fill="both")
        self.lis_frame.pack()
        self.toggleLissajou.pack(side = "left")
        self.place_lissajou_on_secondary.pack(side="right")
        self.scale_spot_index.pack(side="top",expand=1, fill="both")


        self.secondary_screen.packing()

#classe Controller
class Controller(Observer):
    def __init__(self,parent,screen, subject,name, color):
        self.screen = screen
        self.frame = Frame(parent)
        self.subject=subject
        self.visibility = IntVar()
        self.lab = Checkbutton(self.frame,text="Signal " + name, fg = color, command=self.update_visibility, variable = self.visibility)
        self.lab.select()
        self.amp=IntVar()
        self.scale_amp=Scale(self.frame,variable=self.amp,
                          label="Amplitude",
                          orient="horizontal",length=250,
                          showvalue=0,from_=0,to=5,
                          sliderlength=20,tickinterval=1,
                          command=self.update_amplitude)
        self.freq=IntVar()
        self.scale_freq=Scale(self.frame,variable=self.freq,
                          label="Frequence",
                          orient="horizontal",length=250,
                          showvalue=1,from_=0,to=30,
                          sliderlength=20,tickinterval=5,
                          command=self.update_frequency)
        self.phase=DoubleVar()
        self.scale_phase=Scale(self.frame,variable=self.phase,
                          label="Phase",
                          orient="horizontal",length=250,
                          showvalue=0,from_=0,to=5,resolution=0.1,
                          sliderlength=20,tickinterval=1,
                          command=self.update_phase)

    #?
    def update(self,subject):
        pass

    #fonction permettant la mise à jour de l'amplitude
    def update_amplitude(self,event):
        print("update_amplitude(self,event)",self.amp.get())
        self.subject.set_magnitude(self.amp.get())

    #fonction permettant la mise à jour de la fréquence
    def update_frequency(self,event):
        print("update_frequency(self,event)",self.freq.get())
        self.subject.set_frequency(self.freq.get())

    #fonction permettant la mise à jour de la phase
    def update_phase(self,event):
        print("update_phase(self,event)",self.phase.get())
        self.subject.set_phase(self.phase.get())
        
    #fonction permettant l'affichage ou non d'un signal
    def update_visibility(self):
        print("update_visibylity(self)",self.visibility.get())
        self.screen.set_visibility(self.subject, self.visibility.get() == 1)

    def packing(self,side) :
        self.frame.pack(side=side)
        self.lab.pack()
        self.scale_amp.pack()
        self.scale_freq.pack()
        self.scale_phase.pack()

if  __name__ == "__main__" :
    root=Tk()
    model=Generator()
    oscillo=Controller(root,model)
    oscillo.packing()
    root.mainloop()
