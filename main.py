# -*- coding: utf-8 -*-
#!/usr/bin/python3
#importation des différents modules
from tkinter import *
import os
import json
from observer import Observer
from generator import *
import PIL as pil
from screen import *
from controls import *

from tkinter import messagebox
from tkinter import filedialog
from tkinter.filedialog import asksaveasfile
from tkinter.filedialog import askopenfile

from utils import degree_radian

#Classe MenuBar permettant de gérer les différents affichages et fontcions de la barre menu
class MenuBar(object):
    def __init__(self,parent, exitFunc, screen, controls_gen, controls, controls_2):
        self.parent = parent
        self.screen = screen
        self.controls_gen = controls_gen
        self.controls = controls
        self.controls_2 = controls_2
        self.mainMenu = Menu(parent)
        #Construction des différents sous-menus
        #Sous-menus du menu Aide
        menu_1 = Menu(self.mainMenu, tearoff=0)
        menu_1.add_command(label="♠ About us", command=lambda:self.show_About_us(1))
        menu_1.add_command(label="• About Tk", command=lambda:self.show_About_us(2))
        menu_1.add_command(label="• About Python",command=lambda:self.show_About_us(3))

        #Sous-menus Option 
        menu_2 = Menu(self.mainMenu, tearoff=0)
        menu_2.add_command(label="♦ Save data", command=self.save)
        menu_2.add_command(label="♥ Save image", command=self.save_image)

        #Sous-menus du menu OPen
        menu_3 = Menu(self.mainMenu, tearoff=0)
        menu_3.add_command(label="○ Open file", command=self.load)

        #Menus 
        self.mainMenu.add_cascade(label="☼ Open", menu=menu_3)
        self.mainMenu.add_cascade(label="☼ Options", menu=menu_2)
        self.mainMenu.add_cascade(label="☼ Aide", menu=menu_1)
        self.mainMenu.add_command(label="Θ Exit", command=self.ExitApplication)

        #Liste des différents formats pour la sauvegarde de l'image
        self.formats = [('All Files', '*.*'),
                        ('osc files', '*.osc'), 
                        ('Python Files', '*.py'), 
                        ('Image Files save as png', '*.png'),
                        ('Image Files save as jpg', '*.jpg'),
                        ('Text Document', '*.txt')]

    #fonction permettant de s'assurer que l'utilisateur veuille bien quitter l'application lorsque ce dernier clique sur 'exit' de la barre menu
    def ExitApplication(self):
            data = self.open_json()

            MsgBox = messagebox.askquestion('Quitter',data["exitQuestion"], icon = 'warning')
            if MsgBox == 'yes':
                self.parent.destroy()
            else:
                messagebox.showinfo('Retour A l\'Application',data["exitInfo"])

    #fonction permettant quitter l'application
    def leave():
        quit(self.parent)

    #fonction permettant d'afficher les différente infomations sur les concepteurs de l'application, Tkinter et Python selon l'indice donné en paramètre
    def show_About_us(self, x):
            data = self.open_json()
            about_window = Toplevel(self.parent)
            about_window.title(data["about_title"])
            about_window.geometry(data["about_size"])
            # " \n Bakary Konate b0konate@enib.fr \n Alexis Esparvier a7esparv@enib.fr \n Enib © CAI Lab "
            about_window.resizable(False, False)
            if(x==1):
                lb = Label(about_window, text= data["info"]+ 
                                                    "\n" + "\n" + 
                                                    data["Author"][0]["name"] + "\n" + 
                                                    data["Author"][0]["email"] 
                                                    + "\n" + "\n" +
                                                    data["Author"][1]["name"] + "\n" + 
                                                    data["Author"][1]["email"] + "\n" + "\n" + 
                                                    data["copyright"])
            elif (x==2):
                 lb = Label(about_window, text=data["about_tk"])
            elif (x==3):
                 lb = Label(about_window, text=data["about_python"])
            else :
                lb = Label(about_window, text=data["avertiss"])
           
            lb.pack(pady=(40, 0))
    # chargeur de mon fichier json
    def open_json(self):
        with open('data.json') as f:
            data = json.load(f)
            return data

    #fonction permettant de sauvegarder l'image dans un dossier
    def save_image(self):
            # partie  a   ------>  sauvegarder l'image
            self.screen.canvas.postscript(file="figure.eps")
            # img_name = "figure.png"
            img = pil.Image.open("figure.eps")
            # img.save(img_name, "png")
            # partie  b   ------>  sauvegarder dans un repertoire et chsoir son nom
            file = asksaveasfile(title="Donner le nom de l'image", filetypes = self.formats, defaultextension = ".jpg")
            os.remove("figure.eps")
            file_name, file_extension = os.path.splitext(file.name) # nous donner le l'extension du fichier et le nom
            # print(file_name)
            print(file_extension) 
            if file.name.endswith('.png'):
                img.save(file_name+".png", "png")
                # print(file.name)
            else :
                img.save(file) # sauvegarder l'image dans un repertoire selon son nom
                # print(file.name)

    #fonction permettant de sauvegarder un fichier
    def save(self):
        filename = filedialog.asksaveasfilename(parent=self.parent,
                                                filetypes=self.formats,
                                                defaultextension = ".osc",
                                                title="Donner le nom du fichier de sauvegarde...")
        if (len(filename) > 0):
            self.save_conf(filename)

    #fonction permettant de charger un fichier
    def load(self):
        filename = filedialog.askopenfilename(parent=self.parent,
                                                defaultextension = ".osc",
                                                filetypes=self.formats,
                                                title="Donner le nom du fichier de config...")
        if (len(filename) > 0):
            self.load_conf(filename)

    #fonction permettant de sauvegarder la configuration des signaux       
    def save_conf(self,filename):
        #obtention des différents paramètres des signaux
        amp = self.controls.amp.get()
        freq = self.controls.freq.get()
        phase = self.controls.phase.get()
        amp_2 = self.controls_2.amp.get()
        freq_2 = self.controls_2.freq.get()
        phase_2 = self.controls_2.phase.get()
        #sauvegarde des différents paramètres des signaux par l'écriture des paramètres obtenu dans un fichier
        with open(filename,mode="w") as f:
            f.write("Signal 1 : amplitude,frequence,phase\n")
            f.write(str(amp) + "," + str(freq) + "," + str(phase) + "\n")
            f.write("Signal 2 : amplitude,frequence,phase\n")
            f.write(str(amp_2) + "," + str(freq_2) + "," + str(phase_2) + "\n")
            f.close()

    #fonction permettant de charger la configuration des signaux            
    def load_conf(self, filename):
        with open(filename,mode="r") as f:
            # On passe la ligne de commentaire
            f.readline()
            self.extractSigInfos(f.readline(),self.controls)
            f.readline()
            self.extractSigInfos(f.readline(),self.controls_2)
            f.close()

    #fonction permettant d'extraire les infomations voules d'un fichier
    def extractSigInfos(self,line, controls):
        print(line)
        amp,freq,phase = [ float(i) for i in line.split(",") ]
        controls.amp.set(amp)
        controls.freq.set(freq)
        controls.phase.set(phase)
        controls.update_amplitude(None)
        controls.update_frequency(None)
        controls.update_phase(None)

    def packing(self) :
        self.parent.config(menu=self.mainMenu)
        pass

#Classe Osciloscope permettant de gérer l'ensemble des composants (signaux, control, screen, barreMenu)
class Oscilloscope(object) :
    def __init__(self,parent):
        self.parent = parent
        self.contr_frame = Frame(parent)
        self.contr_sousframe = Frame(self.contr_frame)
        self.data_import = MenuBar.open_json(self)

        #couleur des différents signaux
        color = self.data_import["colors"][0] #X 
        color_2 =self.data_import["colors"][1]  #Y
        lissajou_color = self.data_import["colors"][2] #Lissajou

        self.model=Generator()

        self.model_2=Generator()

        self.screen=Screen(parent,self.model, self.model_2, fg = color, fg_2 = color_2)

        self.controls=Controller(self.contr_sousframe,self.screen,self.model, "X", color)
        self.controls_2=Controller(self.contr_sousframe,self.screen,self.model_2, "Y", color_2)

        self.contr_gen = Controller_General(self.contr_frame,self.model,self.model_2, self.screen, lissajou_color)

        self.model.attach(self.screen)
        self.model_2.attach(self.screen)

        self.menu=MenuBar(parent, self.exit,self.screen, self.contr_gen, self.controls, self.controls_2)
        self.parent.protocol("WM_DELETE_WINDOW",self.exit)
        self.parent.bind("<KeyPress-q>", self.quit)

    #fonction permettant de s'assurer que l'utilisateur veuille bien quitter l'application lorsque ce dernier clique la croix rouge de la fenêtre
    def exit(self):

        self.dialog = Toplevel()
        self.dialog.title = "etes vous sur ?"
        frame = Frame(self.dialog)
        lab = Label(self.dialog, text=self.data_import["exitQuestion"])
        #bouton permettant de revenir sur l'application
        btn_cancel = Button(frame, text="retour", command= self.cancel)
        #bouton permettant d'enregistrer la configuration
        btn_save = Button(frame, text="enregistrer", command= self.save_and_quit)
        #bouton permettant de défnitivement quitter l'application
        btn_sure = Button(frame, text="Quitter", command= self.quit)
        btn_cancel.grid(row = 0, column=0)
        Label(frame,text="   ").grid(row=0,column=2)
        btn_save.grid(row = 0, column=3)
        Label(frame,text="   ").grid(row=0,column=4)
        btn_sure.grid(row = 0, column=5)

        lab.pack(side="top", padx=5, pady=5)
        frame.pack(side="bottom", padx=5, pady=5)

        self.dialog.mainloop()

    def save(self):
        self.menu.save()
    def save_img(self):
        self.menu.save_image()
    def cancel(self):
        self.dialog.destroy()
    def quit(self, event = ""):
        self.parent.destroy()
    def save_and_quit(self):
        self.save()
        self.quit()
    def packing(self) :
        self.menu.packing()
        self.screen.packing("top")
        self.contr_gen.packing("top")
        self.controls.packing("left")
        self.controls_2.packing("right")
        self.contr_sousframe.pack(side="bottom")
        self.contr_frame.pack(side="bottom", padx=10, pady=10)

if  __name__ == "__main__" :
    root=Tk()
    root.title(MenuBar.open_json(self=root)["Title"])
    oscillo=Oscilloscope(root)
    oscillo.packing()
    root.mainloop()

