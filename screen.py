# -*- coding: utf-8 -*-
#importation des différents modules
import json
from tkinter import *
from observer import Observer
from generator import *
#classe Screen permettant de gérer les différentes vues de l'application ansi que l'affichage sur ces dernières
class Screen(Observer):
    def __init__(self,parent,subject,subject_2, show_1 = True, show_2 = True, show_lissajou = True, bg="white", fg = "red", fg_2 = "blue", fg_lissajou="green"):
        self.frame = Frame(parent)
        self.data = self.open_json()
        self.subject=subject
        self.subject_2=subject_2
        self.fg = fg
        self.fg_2 = fg_2
        self.fg_lissajou = fg_lissajou
        self.signal_id=None
        self.signal_2_id=None
        self.signal_lissajou_id=None
        self.show_1 = show_1
        self.show_2 = show_2
        self.show_lissajou = show_lissajou
        self.spot_radius = 5
        self.show_spot_1 = True
        self.show_spot_2 = True
        self.show_spot_lissajou = True
        self.spot_1_id=None
        self.spot_2_id=None
        self.spot_lissajou_id=None
        self.spots_index=0
        self.canvas=Canvas(self.frame,bg=bg)
        self.canvas.bind("<Configure>", self.resize_canvas)
        self.height = self.canvas.winfo_reqheight()
        self.width = self.canvas.winfo_reqwidth()

        self.grid(self.subject.t * 10, 10)

        self.labs = Frame(self.frame)
        self.spot_ord_lab_1 = Label(self.labs,
                text="X:(" + self.gen_ord_label_text(self.subject) + ")",fg = self.fg)
        self.spot_ord_lab_2 = Label(self.labs,
                text="Y:(" + self.gen_ord_label_text(self.subject_2) + ")",fg = self.fg_2)

    #fonction permettant l'affichage des coordonées (x,y) des signaux X et Y
    def update_ord_label(self):
        self.spot_ord_lab_1.config(text = "X:(" + self.gen_ord_label_text(self.subject) + ")")
        self.spot_ord_lab_2.config(text = "Y:(" + self.gen_ord_label_text(self.subject_2) + ")")

    # chargeur de mon fichier json
    def open_json(self):
        with open('data.json') as f:
            data = json.load(f)
            return data

    #fonction permettant l'affichage des coordonées (x,y)
    def gen_ord_label_text(self, subject):
        spot_index = self.spots_index
        x,y = subject.get_signal()[spot_index]
        return "x : " + ("{0:.3f}".format(x)) + ", y : " + ("{0:.3f}".format(y))

    #fonction permettant gérer le visibilité des signaux X et Y
    def set_visibility(self, subject, val):
        if subject == self.subject:
            self.show_1 = val
        elif subject == self.subject_2:
            self.show_2 = val
        self.update()

    def set_show_lissajou(self, sl):
        self.show_lissajou = sl
        self.update()

    #fonction permettant d'actualiser l'affichage des signaux selon les nouveaux paramètres choisit par l'utilisateur
    def update(self):
        print("View update")
        signal=self.subject.get_signal()
        signal_2=self.subject_2.get_signal()
        self.signal_id=self.plot_signal(signal,self.signal_id,self.show_1,self.fg)
        self.signal_2_id=self.plot_signal(signal_2,self.signal_2_id,self.show_2,self.fg_2)

        self.signal_lissajou_id=self.plot_lissajou(signal,signal_2, self.signal_lissajou_id,self.show_lissajou, self.fg_lissajou)

        self.update_spots(self.spots_index)
        self.update_ord_label()

    #fonction permettant d'actualiser les spots 
    def update_spots(self, index):
        self.spots_index = index

        signal_1 = self.subject.get_signal()
        signal_2 = self.subject_2.get_signal()
        signal_lis = self.calc_lissajou(signal_1,signal_2)

        self.spot_1_id = self.plot_spot(signal_1,self.spots_index,self.spot_1_id,
            self.show_1 and self.show_spot_1, self.fg)
        self.spot_2_id = self.plot_spot(signal_2,self.spots_index, self.spot_2_id,
            self.show_2 and self.show_spot_2, self.fg_2)
        self.spot_lissajou_id = self.plot_spot(signal_lis,self.spots_index, self.spot_lissajou_id,
                self.show_lissajou and self.show_spot_lissajou, self.fg_lissajou)

        self.update_ord_label()

    #fonction permettant l'affichage des signaux
    def plot_signal(self,signal,signal_id,visibility,color="red"):
        if signal_id!=None :
            self.canvas.delete(signal_id)
            signal_id = None
        if visibility and signal and len(signal)>1:
            plot= self.format_signal_plot(signal)
            signal_id=self.canvas.create_line(plot,fill=color,smooth=1,width=2)
        return signal_id

    def format_signal_plot(self,signal):
            w,h=self.width,self.height
            return [(x*w, h/2.0*(y+1)) for (x, y) in signal]

    #fonction permettant le calcul du signal Liassajou à partir de 2 signaux ici les signaux X et Y
    def calc_lissajou(self,signal_1,signal_2):
        plot = []
        for i in range(len(signal_1)):
            plot.append((signal_1[i][1]/4.+.5,signal_2[i][1]/2.))
        return plot

    #fonction permettant l'affichage de lissajou
    def plot_lissajou(self,signal,signal_2,signal_id,visibility,color="green"):
        sig = self.calc_lissajou(signal, signal_2)
        return self.plot_signal(sig,signal_id,visibility,color)

    #fonction permettant l'affichage des spots
    def plot_spot(self,signal,spot_index,spot_id, visibility, color="red"):
        if spot_id != None:
            self.canvas.delete(spot_id)
            spot_id = None
        if visibility and signal and len(signal)>1:
            formated_signal = self.format_signal_plot(signal)
            (x,y) = formated_signal[spot_index]
            coords = (
                x-self.spot_radius,
                y-self.spot_radius,
                x+self.spot_radius,
                y+self.spot_radius
            )
            spot_id = self.canvas.create_oval(coords,fill=color,outline=color)
        return spot_id

    #fonction permettant de modififer la taille du canevas si l'utilisateur modife la taille de la fenêtre
    def resize_canvas(self,event):
        wscale = float(event.width)/self.width
        hscale = float(event.height)/self.height
        self.width = event.width
        self.height = event.height
        self.canvas.scale("all",0,0,wscale,hscale)

    #fonction définissant la grille 
    def grid(self, x_steps, y_steps = -1):
        if (y_steps == -1):
            y_steps = x_steps
        w,h=self.width,self.height
        width,height=int(w),int(h)
        x_step=width/(x_steps*1.)
        for t in range(1,x_steps):
            x =t*x_step
            self.canvas.create_line(x,1,x,height-1, tags="grid", fill=self.data["colors"][4])
        y_step=height/(y_steps*1.)
        for t in range(1,y_steps):
            y =t*y_step
            self.canvas.create_line(1,y,width-1,y, tags="grid", fill=self.data["colors"][4])
        self.canvas.create_line(5,height/2.,width - 5,height/2.,arrow="last", fill=self.data["colors"][0])
        self.canvas.create_line(width/2.,height-5,width/2.,5,arrow="last", fill=self.data["colors"][0])

    def packing(self,side="top") :
        self.frame.pack(expand=1,fill="both",side=side)
        self.canvas.pack(expand=1,fill="both")
        self.labs.pack(expand=1, fill="both", padx=10)
        self.spot_ord_lab_1.pack(side="left")
        self.spot_ord_lab_2.pack(side="right")

if  __name__ == "__main__" :
    root=Tk()
    model=Generator()
    model_2=Generator()
    screen=Screen(root,model, model_2, fg = "red", fg_2 = "blue")
    screen.grid(10,5)
    screen.packing()
    root.mainloop()
