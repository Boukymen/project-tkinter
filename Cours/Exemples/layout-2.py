# Initialisation 
import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
# Actions : definition des comportements
if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    frame=tk.Frame(mw,bg="yellow")
    # frame.pack()
    frame.pack(fill="x",expand=1)
    labelNom=tk.Label(frame,text="Nom :")
    labelPrenom=tk.Label(frame,text="Prenom :")
    entryNom=tk.Entry(frame)
    entryPrenom=tk.Entry(frame)
# IHM : Gestionnaires de positionnement (layout manager)
    frame.pack()
    labelNom.grid(row=0)
    labelPrenom.grid(row=1)
    entryNom.grid(row=0,column=1)
    entryPrenom.grid(row=1,column=1)
# Interaction : liaison Composant-Evenement-Action 
    mw.mainloop()
    exit(0)