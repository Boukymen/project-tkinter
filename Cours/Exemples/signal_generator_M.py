from math import pi,sin
from observer import *

class Generator(Subject) :
    def __init__(self,name="signal"):
        Subject.__init__(self)
        self.name=name
        self.signal=[]
        self.a,self.f,self.p=1.0,1.0,0.0
    def vibration(self,t,harmoniques=1):
        a,f,p=self.a,self.f,self.p
        somme=0
        for h in range(1,harmoniques+1) :
            somme=somme + (a/h)*sin(2*pi*(f*h)*t-p)
        return somme
    def generate_signal(self,period=1,samples=1000):
        del self.signal[0:]
        duration=range(samples)
        Te = period/samples
        for t in duration :
            self.signal.append([t*Te,self.vibration(t*Te)])
        self.notify()
        return self.signal

if __name__ =="__main__" :
    generator=Generator()
    print(generator.generate_signal())
