# Initialisation 
import sys
from math import *
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
# Actions : definition des comportements
def evaluer(event):
    label.configure(text= "Resultat = " + str(eval(entry.get())))
if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    entry=tk.Entry(mw)
    label=tk.Label(mw)
# IHM : Gestionnaires de positionnement (layout manager)
    entry.pack()
    label.pack()
# Interaction : liaison Composant-Evenement-Action
    entry.bind("<Return>",evaluer)
    mw.mainloop()
    exit(0)
