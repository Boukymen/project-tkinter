# Initialisation 
import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
# Actions : definition des comportements

def callback(event) :
    # action to do need information from :
    # - user: get data from pointer, keyboard...
    # - widget: get or set widget data
    # - application: manage data application
    print(dir(event))
    print("pointer coordinates on screen ", event.x_root,event.y_root)
    event.widget.configure(text="x="+str(event.x) +"y="+str(event.y))

if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    hello=tk.Label(mw,text="Hello World !",fg="blue")
    quit=tk.Button(mw,text="Goodbye World",fg="red", command=mw.destroy)
# IHM : Gestionnaires de positionnement (layout manager)
    hello.pack()
    quit.pack()
# Interaction : liaison Composant-Evenement-Action 
    hello.bind("<Button-1>",callback)
    mw.mainloop()
    exit(0)
