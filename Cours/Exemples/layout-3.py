# Initialisation 
import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
# Actions : definition des comportements
if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    mw.title("Layout Manager : Place")
    var = tk.StringVar()
    msg = tk.Message(mw, textvariable=var, bg="yellow",relief="ridge")
    var.set("Place : \n options de  positionnement  de widgets ")
    okButton=tk.Button(mw,text="OK")
# IHM : Gestionnaires de positionnement (layout manager)
    msg.place(relx=0.5,rely=0.5,relwidth=0.75,relheight=0.50,anchor="center")
    okButton.place(relx=0.5,rely=1.05,in_=msg,anchor="n")
    # Interaction : liaison Composant-Evenement-Action 
    mw.mainloop()
    exit(0)
