# Initialisation 
import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
# Actions : definition des comportements
def callback(event) :
    mw.destroy()

if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    hello=tk.Label(mw,text="Hello World !",fg="blue")
    quit=tk.Button(mw,text="Goodbye World",fg="red")
# IHM : Gestionnaires de positionnement (layout manager)
    hello.pack()
    quit.pack()
# Interaction : liaison Composant-Evenement-Action
    quit.bind("<Motion>",callback)
    mw.mainloop()
    exit(0)
