# Initialisation 
import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 

from observer import *
class Model(Subject):
     def __init__(self, names=[]):
         Subject.__init__(self)
         self.__data = names
     def get_data(self):
         return self.__data
     def insert(self,name):
         self.__data.append(name)
         self.notify()
     def delete(self, index):
         del self.__data[index]
         self.notify()
class View(Observer):
     def __init__(self,parent):
          self.parent=parent
          self.list=tk.Listbox(parent)
          self.list.configure(height=4)
          self.list.pack()
          self.entry=tk.Entry(parent)
          self.entry.pack()
     def update(self,model):
         self.list.delete(0, "end")
         for data in model.get_data():
             self.list.insert("end", data)

class Controller(object):
     def __init__(self,model,view):
          self.model,self.view = model,view
          self.view.entry.bind("<Return>",
                               self.enter_action)
          self.view.list.bind("<Delete>",
                              self.delete_action)
     def enter_action(self, event):
          data = self.view.entry.get()
          self.model.insert(data)
     def delete_action(self, event):
          for index in self.view.list.curselection():
             self.model.delete(int(index))

if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    mw.title("Men")
    names=["Jean", "John", "Joe"]
    model = Model(names)
    view = View(mw)
    view.update(model)
    model.attach(view)
    ctrl = Controller(model,view)
    top = tk.Toplevel()
    top.title("Men")
    view = View(top)
    view.update(model)
    model.attach(view)
    ctrl = Controller(model,view)
    top = tk.Toplevel()
    top.title("Women")
    names=["Jeanne", "Joanna", "Jeanette"]
    model = Model(names)
    view = View(top)
    view.update(model)
    model.attach(view)
    ctrl = Controller(model,view)
    mw.mainloop()
    exit(0)
