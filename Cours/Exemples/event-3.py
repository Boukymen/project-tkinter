# Initialisation 
import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
# Actions : definition des comportements
def mouse_location(event,label):
    label.configure(text= "Position X ="+ str(event.x) + ", Y =" + str(event.y))

if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    canvas=tk.Canvas(mw,width=200,height=150,bg="light yellow")
    data=tk.Label(mw,text="Mouse Location")
    hello=tk.Label(mw,text="Hello World !",fg="blue")
    quit=tk.Button(mw,text="Goodbye World",fg="red", command=mw.destroy)
# IHM : Gestionnaires de positionnement (layout manager)
    hello.pack()
    canvas.pack()
    data.pack()
    quit.pack()
# Interaction : liaison Composant-Evenement-Action 
    canvas.bind("<Motion>",lambda event,label=data : mouse_location(event,label))

    mw.mainloop()
    exit(0)
