# Initialisation 
import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
# Actions : definition des comportements
def display(event):
    print("display()")
    x=int(entry.get())
    canvas.create_rectangle(x,x,x+10,x+10,fill="blue")
def set_value(event):
    print("set_value()")
    canvas.event_generate('<Control-Z>')
if __name__ =="__main__" :
# IHM : creation des composants
    mw=tk.Tk()
    canvas=tk.Canvas(mw,width=100,height=200,bg="white",bd=1)
    label=tk.Label(mw,text = "Valeur :")
    entry=tk.Entry(mw)
# IHM : Gestionnaires de positionnement (layout manager)
    canvas.pack()
    label.pack(side="left")
    entry.pack(side="left")
# Interaction : liaison Composant-Evenement-Action 
    mw.bind("<Control-Z>",display)
    entry.bind("<Return>",set_value)
    mw.mainloop()
    exit(0)
