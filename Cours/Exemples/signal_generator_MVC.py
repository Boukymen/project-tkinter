import sys
major=sys.version_info.major
minor=sys.version_info.minor
if major==2 and minor==7 :
    import Tkinter as tk
    import tkFileDialog as filedialog
elif major==3 and minor==6 :
    import tkinter as tk
    from tkinter import filedialog
else :
    print("Your python version is : ",major,minor)
    print("... I guess it will work !")
    import tkinter as tk
    from tkinter import filedialog 
from math import pi,sin
from observer import *

class Generator(Subject) :
    def __init__(self,name="signal"):
        Subject.__init__(self)
        self.name=name
        self.signal=[]
        self.a,self.f,self.p=1.0,1.0,0.0
    def get_name(self) :
        return self.name
    def get_signal(self) :
        return self.signal
    def set_magnitude(self,a) :
        self.a=a
    def set_frequency(self,f) :
        self.f=f
    def vibration(self,t,harmoniques=1):
        a,f,p=self.a,self.f,self.p
        somme=0
        for h in range(1,harmoniques+1) :
            somme=somme + (a/h)*sin(2*pi*(f*h)*t-p)
        return somme
    def generate_signal(self,period=1,samples=1000):
        del self.signal[0:]
        duration=range(samples)
        Te = period/samples
        for t in duration :
            self.signal.append([t*Te,self.vibration(t*Te)])
        self.notify()
        return self.signal
class Screen(Observer):
    def __init__(self,parent,bg="white",width=600,height=300):
        Observer.__init__(self)
        self.canvas=tk.Canvas(parent,bg=bg,width=width,height=height)
        self.signals={}
        self.width,self.height=width,height
        self.canvas.bind("<Configure>",self.resize)
    def update(self,subject=None):
        print(self.signals.keys())
        if subject.get_name() not in self.signals.keys() :
            self.signals[subject.get_name()]= subject.get_signal()
        else :
            print(subject.get_name())
            self.canvas.delete(subject.get_name())
        self.plot_signal(subject.get_signal(),subject.get_name())
    def plot_signal(self,signal,name,color="red"):
        w,h=self.width,self.height
        if signal and len(signal) > 1:
            plot = [(x*w,h/2.0*(1-y)) for (x, y) in signal]
            self.canvas.create_line(plot,fill=color,smooth=1,width=2,tags=name)
        return 
    def grid(self,tiles=8):
        tile_x=self.width/tiles
        for t in range(1,tiles+1):
            x =t*tile_x
            self.canvas.create_line(x,0,x,self.height,tags="grid")
            self.canvas.create_line(x,self.height/2-10, x,self.height/2+10,width=3,tags="grid")
        tile_y=self.height/tiles
        for t in range(1,tiles+1):
            y =t*tile_y
            self.canvas.create_line(0,y,self.width,y,tags="grid")
            self.canvas.create_line(self.width/2-10,y, self.width/2+10,y, width=3,tags="grid")
    def resize(self,event):
        if event:
            self.width,self.height=event.width,event.height
            self.canvas.delete("grid")
            for name in self.signals.keys():
                self.canvas.delete(name)
            self.plot_signal(self.signals[name],name)
            self.grid()
    def packing(self) :
        self.canvas.pack(expand=1,fill="both",padx=6)
class Controller :
    def __init__(self,parent,model,view):
        self.model=model
        self.view=view
        self.create_controls(parent)
    def create_controls(self,parent):
        self.frame=tk.LabelFrame(parent,text='Signal')
        self.amp=tk.IntVar()
        self.amp.set(1)
        self.scaleA=tk.Scale(self.frame,variable=self.amp,
                             label="Amplitude",
                             orient="horizontal",length=250,
                             from_=0,to=5,tickinterval=1)
        self.scaleA.bind("<Button-1>",self.update_magnitude)
    def update_magnitude(self,event):
        self.model.set_magnitude(self.amp.get())
        self.model.generate_signal()
    def packing(self) :
        self.frame.pack()
        self.scaleA.pack()

if __name__ =="__main__" :
    mw=tk.Tk()
    model=Generator()
    view=Screen(mw)
    view.grid(8)
    view.packing()
    model.attach(view)
    model.generate_signal()
    ctrl=Controller(mw,model,view)
    ctrl.packing()
    model=Generator("another_signal")
    model.attach(view)
    model.set_frequency(3)
    ctrl=Controller(mw,model,view)
    ctrl.packing()
    top=tk.Toplevel()
    # view=Screen(top)
    # view.grid(10)
    # model.attach(view)
    # view.update(model)
    # ctrl=Controller(top,model,view)
    # view.packing()
    # ctrl.packing()
    mw.mainloop()
