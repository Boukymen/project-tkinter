# -*- coding: utf-8 -*-
#importation des différents modules
from math import sin,pi
from utils import degree_radian

from observer import Subject

#classe Generator
class Generator(Subject) :
    def __init__(self,a=0.0,f=0.0,p=0.0,t=1):
        Subject.__init__(self)
        self.signal=[]
        self.a,self.f,self.p,self.t=a,f,p,t
        self.generate_signal()

    def get_signal(self):
        return self.signal

    def get_magnitude(self):
        return self.a

    def set_magnitude(self,a):
        self.a=a
        self.generate_signal()

    def get_frequency(self):
        return self.f

    def set_frequency(self,f):
        self.f=f
        self.generate_signal()

    def get_phase(self):
        return self.p

    def set_phase(self,p):
        self.p=p
        self.generate_signal()

    #fonction générant un signal
    def generate_signal(self):
        del self.signal[0:]
        samples=1000
        for t in range(0, samples,5):
            samples=float(samples)
            e=self.a*sin((2*pi*self.f*self.t*(t*1.0/samples))-self.p)
            self.signal.append((t*1.0/samples,e))
        
        self.notify()
        return self.signal

